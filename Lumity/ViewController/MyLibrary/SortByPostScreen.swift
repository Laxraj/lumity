//
//  SortByPostScreen.swift
//  Lumity
//
//  Created by Nikunj Vaghela on 08/02/24.
//

import UIKit
import PanModal

class SortByPostScreen: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var sortByTableView: UITableView!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    let itemArray: [LibrarySortByRequest] = [LibrarySortByRequest(option: "Newest"),LibrarySortByRequest(option: "Oldest")]
    
    var sortIndex: Int = 0
    weak var delegate: LibrarySortByDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        panModalSetNeedsLayoutUpdate()
        self.sortByTableView.dataSource = self
        self.sortByTableView.delegate = self
        self.sortByTableView.register(UINib(nibName: "LibrarySortByCell", bundle: Bundle.main), forCellReuseIdentifier: "LibrarySortByCell")
    }

}
//MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension SortByPostScreen: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.sortByTableView.dequeueReusableCell(withIdentifier: "LibrarySortByCell", for: indexPath) as! LibrarySortByCell
        cell.item = self.itemArray[indexPath.row]
        cell.selectedView.isHidden = !(self.sortIndex == indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sortIndex = indexPath.row
        tableView.reloadData()
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.selectSort(sortBy: self?.sortIndex == 0 ? .NEWEST : .OLDEST)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            self?.tableViewHeightConstraint.constant = tableView.contentSize.height
        }
    }
}
//MARK: - PANMODAL DELEGATE
extension SortByPostScreen: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var panScrollable: UIScrollView? {
        return nil
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(200)
    }

    var longFormHeight: PanModalHeight {
        return .contentHeight(200)
    }
    
    var allowsExtendedPanScrolling: Bool{
        return true
    }
    
    var showDragIndicator: Bool{
        return false
    }
    
    
//    var panModalBackgroundColor: UIColor{
//        return Utility.getUIcolorfromHex(hex: "eaeaea")
//    }
}
