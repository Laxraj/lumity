//
//  MessageView.swift
//  Source-App
//
//  Created by Nikunj on 13/06/21.
//
import UIKit
import Foundation

class MessageView: NibView{
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImageView: dateSportImageView!
    @IBOutlet weak var messageLabel: UILabel!
}
