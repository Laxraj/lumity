//
//  LibrarySortByCell.swift
//  Lumity
//
//  Created by Nikunj Vaghela on 08/02/24.
//

import UIKit

class LibrarySortByCell: UITableViewCell {
    
    @IBOutlet weak var optionLabel: UILabel!
    
    @IBOutlet weak var selectedView: dateSportView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: LibrarySortByRequest?{
        didSet{
            self.optionLabel.text = item?.option
        }
    }
}
