//
//  LoginIntroCell.swift
//  Source-App
//
//  Created by Nikunj on 24/03/21.
//

import UIKit

class LoginIntroCell: UICollectionViewCell {

    @IBOutlet weak var introImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
