//
//  Protocol.swift
//  Source-App
//
//  Created by Nikunj on 11/04/21.
//

import Foundation


protocol SelectedIntrestDelegate: NSObject {
    func getIntrestData(data: [InterestsListData])
}

protocol EnterNewPostDelegate: NSObject {
    func uploadNewPost()
}

protocol DeleteGroupDelegate: NSObject {
    func deleteGroup()
}

protocol LibrarySortByDelegate: NSObject{
    func selectSort(sortBy: FilterType)
}
